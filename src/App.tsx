import React, { Component } from "react"
import { Button, ControlGroup, EditableText, FormGroup, InputGroup } from "@blueprintjs/core"
import "./App.sass"
import { AppToaster } from "./Toaster"

interface Props {
}

interface State {
  password: string
  filepath: string
  content: string
  loadLoading: boolean
  saveLoading: boolean
  reloadLoading: boolean
}

class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      password: "",
      filepath: "/root/deployer/deployer.conf",
      content: "",
      loadLoading: false,
      saveLoading: false,
      reloadLoading: false
    }
  }

  load() {
    this.setState({ loadLoading: true })
    const query = `?path=${this.state.filepath}&password=${this.state.password}`
    fetch("/files/read" + query)
      .then(resp => resp.text())
      .then(content => {
        AppToaster.show({ message: "Loaded", intent: "success", icon: "tick" })
        this.setState({ content, loadLoading: false })
      })
      .catch(err => {
        AppToaster.show({ message: "Not loaded", intent: "danger", icon: "cross" })
        this.setState({ content: "", loadLoading: false })
        throw err
      })
  }

  save() {
    this.setState({ saveLoading: true })

    const query = `?path=${this.state.filepath}&password=${this.state.password}`

    const body = {
      method: "POST",
      body: this.state.content
    }

    fetch("/files/write" + query, body)
      .then(resp => {
        this.setState({ saveLoading: false })
        AppToaster.show({ message: "Saved", intent: "success", icon: "tick" })
      })
      .catch(err => {
        AppToaster.show({ message: "Not saved", intent: "danger", icon: "cross" })
        this.setState({ saveLoading: false })
        throw err
      })
  }

  reload() {
    this.setState({ reloadLoading: true })
    fetch("/reload")
      .then(resp => resp.text())
      .then(content => {
        this.setState({ content, reloadLoading: false })
        AppToaster.show({ message: "Reloaded", intent: "success", icon: "tick" })
      })
      .catch(err => {
        AppToaster.show({ message: "Not reloaded", intent: "danger", icon: "cross" })
        this.setState({ content: "", reloadLoading: false })
        throw err
      })
  }

  render() {
    return (
      <div className={"app-container"}>
        <FormGroup label={"Password"}>
          <InputGroup
            fill
            type={"password"}
            placeholder={"secret"}
            value={this.state.password}
            onChange={(ev: any) => this.setState({ password: ev.target.value })}
          />
        </FormGroup>

        <FormGroup label={"File path"}>
          <ControlGroup fill>
            <InputGroup
              fill
              placeholder={"/root/deployer/deployer.conf"}
              value={this.state.filepath}
              onChange={(ev: any) => this.setState({ filepath: ev.target.value })}
            />
            <Button
              text={"Load"}
              intent={"primary"}
              onClick={this.load.bind(this)}
              loading={this.state.loadLoading}
            />
            <Button
              text={"Save"}
              intent={"success"}
              onClick={this.save.bind(this)}
              loading={this.state.saveLoading}
            />
            <Button
              text={"Reload"}
              intent={"danger"}
              onClick={this.reload.bind(this)}
              loading={this.state.reloadLoading}
            />
          </ControlGroup>
        </FormGroup>

        <FormGroup label={"Content"}>
          <EditableText
            multiline={true}
            minLines={27}
            maxLines={27}
            value={this.state.content}
            onChange={content => this.setState({ content })}
          />
        </FormGroup>
      </div>
    )
  }
}

export default App
